# Django Login Basic

## Este projeto foi feito com:

* [Python 3.11.7](https://www.python.org/)
* [Django 5.0.4](https://www.djangoproject.com/)
* [Django-Ninja 1.1.0](https://django-ninja.dev/)

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/rg3915/django-login-basic.git
cd django-login-basic

python -m venv .venv
source .venv/bin/activate  # Linux
# .venv\Scripts\activate  # Windows

pip install -r requirements.txt

python contrib/env_gen.py

python manage.py migrate
python manage.py createsuperuser --username="admin" --email=""
```

## Links

https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html

![](img/model.png)