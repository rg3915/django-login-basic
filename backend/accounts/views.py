from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login
from django.http import HttpResponse

from .models import Profile
from .forms import LoginForm, RegistrationForm


def login_view(request):
    template_name = 'accounts/login.html'
    form = LoginForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            cpf = form.cleaned_data['cpf']
            password = form.cleaned_data['password']

            profile = get_object_or_404(Profile, cpf=cpf)
            if profile is not None:
                user = profile.user

                # Autentica o usuário
                user = authenticate(request, username=user.username, password=password)

            if user is not None:
                # Faz login
                auth_login(request, user)
                # return HttpResponse(f'Boas vindas {user.first_name}')
                return redirect('core:index')
            else:
                # login inválido
                return HttpResponse('Login inválido.')

    context = {'form': form}
    return render(request, template_name, context)


def registration(request):
    template_name = 'accounts/registration.html'
    form = RegistrationForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('core:index')

    context = {'form': form}
    return render(request, template_name, context)
