from django.urls import path
from django.contrib.auth.views import LogoutView

from backend.accounts import views as v


urlpatterns = [
    path('register/', v.registration, name='registration'),
    path('login/', v.login_view, name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
]
