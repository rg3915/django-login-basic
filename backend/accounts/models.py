from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    class ProfileType(models.TextChoices):
        ALUNO = 'A', 'Aluno'
        GESTOR = 'G', 'Gestor'

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        verbose_name='usuário',
    )
    cpf = models.CharField('CPF', max_length=14, unique=True, null=True)
    telefone = models.CharField(max_length=20, null=True, blank=True)
    tipo_perfil = models.CharField(
        'tipo de perfil',
        max_length=1,
        choices=ProfileType.choices,
        default=ProfileType.ALUNO,
    )

    class Meta:
        ordering = ('user__first_name',)
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfis'

    @property
    def full_name(self):
        return f'{self.user.first_name} {self.user.last_name or ""}'.strip()

    def __str__(self):
        return self.full_name


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
