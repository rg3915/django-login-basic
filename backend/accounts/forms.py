from django import forms
from django.contrib.auth.models import User

from .models import Profile


class LoginForm(forms.Form):
    cpf = forms.CharField(max_length=14, label='CPF')
    password = forms.CharField(widget=forms.PasswordInput, label='Senha')


class RegistrationForm(forms.ModelForm):
    username = forms.CharField(
        label='Usuário',
        max_length=150,
    )
    first_name = forms.CharField(
        label='Nome',
        max_length=150,
    )
    last_name = forms.CharField(
        label='Sobrenome',
        max_length=150,
    )
    email = forms.EmailField(
        label='E-mail',
    )
    password1 = forms.CharField(widget=forms.PasswordInput, label='Senha')
    password2 = forms.CharField(widget=forms.PasswordInput, label='Confirmação da Senha')

    class Meta:
        model = Profile
        fields = ('username', 'first_name', 'last_name', 'email', 'cpf', 'telefone')

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']

            if password1 == password2:
                return password2

        raise forms.ValidationError('As senhas são diferentes.')

    def save(self, commit=True):
        instance = super().save(commit=False)

        data = self.cleaned_data
        username = data['username']
        first_name = data['first_name']
        last_name = data['last_name']
        email = data['email']

        password1 = data['password1']

        cpf = data['cpf']
        telefone = data['telefone']

        user = User.objects.create_user(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=email,
            password=password1,
        )

        user.profile.cpf = cpf
        user.profile.telefone = telefone
        user.profile.save()

        return instance
