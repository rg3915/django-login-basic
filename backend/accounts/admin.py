from django.contrib import admin

from .models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'get_email', 'cpf', 'tipo_perfil')
    search_fields = ('cpf',)
    list_filter = ('tipo_perfil',)

    @admin.display(description='E-mail')
    def get_email(self, obj):
        return obj.user.email
