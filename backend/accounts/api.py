from http import HTTPStatus
from ninja import Router, ModelSchema
from django.contrib.auth.models import User

from .models import Profile


router = Router(tags=['Accounts'])


class UserSchema(ModelSchema):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileSchema(ModelSchema):
    # nested schema
    user: UserSchema

    class Meta:
        model = Profile
        fields = ('cpf', 'telefone')


@router.get('/profile', response=list[ProfileSchema])
def list_profile(request):
    return Profile.objects.all()


@router.post('/login')
def login_api(request, payload):
    """
    Criar uma endpoint com o métod post
    Seu login já está pronto, é só adaptar.

    O payload deve conterm cpf e password.
    """
